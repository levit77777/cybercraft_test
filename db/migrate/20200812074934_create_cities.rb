class CreateCities < ActiveRecord::Migration[6.0]
  def change
    create_table :cities do |t|
      t.string :name, null: false, index: true
      t.integer :population, null: false, index: true

      t.timestamps
    end
  end
end
