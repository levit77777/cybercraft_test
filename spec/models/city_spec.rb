require 'rails_helper'

RSpec.describe City, type: :model do

  City.delete_all
  City.create(name: "Xxx", population: 1) 
  City.create(name: "Aabab", population: 777)
  City.create(name: "Qwerty", population: 1010740)
  City.create(name: "Cbaca", population: 85)
  City.create(name: "Xaax", population: 42)
  City.create(name: "Xcxaa", population: 9)

  it "filter by search word or similar" do

    expect(City.name_similar_to("aA").pluck(:name)).to eq ["Aabab", "Xaax", "Xcxaa"]
  end

  it "order by population asc" do
    expect(City.ordered_by("population").pluck(:population)).to eq [1, 9, 42, 85, 777, 1010740]
  end

  it "order by population desc" do
    expect(City.ordered_by("population", "desc").pluck(:population)).to eq [1010740, 777, 85, 42, 9, 1]
  end

  it "order by name asc" do
    expect(City.ordered_by("name").pluck(:name)).to eq ["Aabab", "Cbaca", "Qwerty", "Xaax", "Xcxaa", "Xxx"]
  end

  it "order by name desc" do
    expect(City.ordered_by("name", "desc").pluck(:name)).to eq ["Xxx", "Xcxaa", "Xaax", "Qwerty", "Cbaca", "Aabab"]
  end

end
