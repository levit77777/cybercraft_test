class CitiesController < ApplicationController

  def index
    @cities = City.name_similar_to(params[:name])
                  .ordered_by(params[:order_by], params[:desc])
                  .page(params[:page])
  end

end
