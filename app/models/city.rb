class City < ApplicationRecord
  paginates_per 5

  scope :name_similar_to, ->(name) { where("name ilike ?", "%#{name}%") if name }
  scope :ordered_by, ->(order_by, desc=false) do 
    direction = desc ? :desc : :asc
    case order_by
      when "population"
        order(population: direction)
      when "name"
        order(name: direction)
    end
  end
end
